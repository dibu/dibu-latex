TEXMFHOME := $(shell kpsewhich -var-value=TEXMFHOME)
PKG_NAME := dibu-macros

install:
	@echo "Installing $(PKG_NAME) into $(TEXMFHOME)...\n"
	mkdir -p "$(TEXMFHOME)/tex/latex/dibu/"
	cp dibu-macros.sty "$(TEXMFHOME)/tex/latex/dibu/"
	@echo "Done!"
	@echo "\nUsage: \usepackage{dibu-macros}"

