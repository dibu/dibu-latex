# dibu-latex

## Description
This is my personal collection of macros I find useful for everyday LaTeX usage.


## Installation
Simply clone the git repository, and run
```bash
  make install
```
This assumes an installation of TeX-Live, in particular, that `kpsewhich` command is installed.
Otherwise, you may manually copy the relevant `.sty` files.


